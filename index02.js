const express = require('express');
const port = 8000;

let app = express()
app.get('/', (request, response) => {
    response.send("Resultado sucesso raíz");
    console.log("GET /");
});

//CAMINHO DOS CLIENTES
app.get('/clientes',(request,response) => {
    let obj = request.query;
    response.send("Resultado sucesso /CLIENTES " + obj.nome);
    console.log("GET /CLIENTES");
    //http://localhost:8000/clientes/?nome=Marcelo&Sobrenome=Caetano&idade=19
});

app.listen(port,() => {
    console.log(`Projeto executando na porta ${port}`);
})